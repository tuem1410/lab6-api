﻿using Microsoft.EntityFrameworkCore;

namespace Lab6_2180603186.Models
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext>
        options) : base(options)
        { }
        public DbSet<Product> Products { get; set; }
    }
}
